import React, { Component } from 'react';
import Footer from './partials/Footer';
import Header from './partials/Header';
import DownloadApp from './partials/DownloadApp';
import Connect from './partials/Connect';
import Faq from './partials/Faq'; 
import Reviews from './partials/Reviews'

class Home extends Component{
    render(){
        return(
            <div>
                <Header />
                <Reviews />  
                <DownloadApp />
                <Faq />
                <Connect /> 
                <Footer />
            </div>
        )
    }
}

export default Home;