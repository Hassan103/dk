import React , { Component } from 'react';
import message from './../../assets/images/message.png';
import phoneicon from './../../assets/images/phoneicon.png';
import location from './../../assets/images/location.png';
import mascot from './../../assets/images/mascot.png';
import mosMessage from './../../assets/images/mosMessage.png';
import CS1 from './../../assets/images/CS1.png';

import whitePlus from './../../assets/images/whitePlus.png';




class Connect extends Component{
    constructor(props){
        super(props); 
    }

    render(){
        return(
            <section className="lets-connects-wrapper">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="lets-text-area">
                                <h1 className="section-heading">Let’s Connect</h1>
                                <div className="connect-contacts">
                                    <ul className="no-default">
                                        <li><span className="icon icon-1"> <img src={ message } /> </span><a href="mailto:hello@doclink.health">hello@doclink.health</a></li>
                                        <li><span className="icon icon-2"> <img src={ phoneicon } /> </span> <a href="tel:+92-302-8292814">+92-302-8292814</a></li>
                                        <li>
                                            <div>
                                            <span className="icon icon-3"> <img src={ location } /> </span>2C Plot 8C, Sunset Lane 9, Phase 2
                                                <span className="add"> Commercial Area Defense Housing </span>
                                                <span className="add"> Authority, Karachi, Sindh, 75500</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>                                
                            </div>
                            <div className="google-maps">
                                <div className="maps-wrapper">
                                    <div class="mapouter">
                                        <div class="gmap_canvas">
                                            <iframe width="70%" height="350" id="gmap_canvas" 
                                            src="https://maps.google.com/maps?q=doclink&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                            frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                                            </iframe>
                                            {/* <a href="https://putlocker-is.org"></a><br/>*/}
                                            {/* <a href="https://www.embedgooglemap.net">html google map</a> */}
                                        </div>
                                    </div>       
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 lets-right-side">
                            {/* <div className="moscat-message">
                                <p>
                                    “We want to bring peace of mind to our customers by connecting them to their trusted physicians”
                                </p>
                            </div> */}
                            <div className="blue-plus">
                                <img src={ CS1 } />
                            </div>  
                            <div className="blue-plus-white">
                                <img src={ whitePlus } />
                            </div>  
                            <div className="mos-message">
                                {/* <img src={ mosMessage } /> */}
                                <p>" We want to bring peace of mind to our customers by connecting them to their trusted physicians " </p>
                            </div>  
                            <div className="mascot">
                                <img src={ mascot } />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Connect;
