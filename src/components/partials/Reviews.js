import React, { Component, Fragment } from 'react';
import d01 from './../../assets/images/d01.jpeg';
import d02 from './../../assets/images/d02.jpg';
import whitePlus from './../../assets/images/whitePlus.png';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

class Reviews extends Component{
    constructor(props){
        super(props);
        this.state = {
            isActive : 'doctor'
        }
    }


    // Change Tabs Functions

    changeTabs = (event) => {
        let get_id = event.target.attributes.getNamedItem('data-id').value; 
        this.setState({
            isActive : get_id
        })
    }


    
    render(){
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            nav:false,
            arrows:true,
            // className: "center",
            // centerMode: true,
            // centerPadding: "60px",
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
              ]
          };
        return(
            <Fragment>
                <section className="reviews-section">
                    <div className="container-fluid">
                        <div className="row">    
                            <div className="col-md-12">
                                <div className="main-tabs">
                                    <div className="tabs-header">
                                        <ul className="no-default">
                                            <li className={ this.state.isActive == "doctor" ? "active" : "" } data-id="doctor" onClick = { this.changeTabs } > Doctors </li>
                                            <li className={ this.state.isActive == "patient" ? "active" : "" } data-id="patient" onClick = { this.changeTabs } > Patients </li>
                                        </ul>
                                    </div>
                                    <div className="tabs-body">
                                        <div className="tabs-inner">
                                         
                                            {
                                                this.state.isActive == "doctor" ? 
                                                   //Doctor 
                                                <div className="doctor-list">
                                                    <Slider {...settings}>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                                <div className="reviews-text">
                                                                    <p>
                                                                    “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                    Velit officia consequat duis enim velit mollit.
                                                                    </p>
                                                                </div>
                                                                <div className="reviews-details">
                                                                    <div className="review-img">
                                                                        <img src={ d01 } />
                                                                    </div>
                                                                    <div className="reviews-names">
                                                                        <h5>Dr. Amna Chaudhry</h5>
                                                                        <p>Ophthalmologist AKUH</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                                <div className="reviews-text">
                                                                    <p>
                                                                    “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                    Velit officia consequat duis enim velit mollit.
                                                                    </p>
                                                                </div>
                                                                <div className="reviews-details">
                                                                    <div className="review-img">
                                                                        <img src={ d01 } />
                                                                    </div>
                                                                    <div className="reviews-names">
                                                                        <h5>Dr. Amna Chaudhry</h5>
                                                                        <p>Ophthalmologist AKUH</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                Velit officia consequat duis enim velit mollit.
                                                                
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d02 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                Velit officia consequat duis enim velit mollit.
                                                               
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d02 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                Velit officia consequat duis enim velit mollit.
                                                                
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </Slider> 
                                                 
                                                </div> 
                                                
                                                //END DOCTOR TAB's
                                            
                                                : 
                                                //Patient TAB's
                                                <div className="patient-list">
                                                    <Slider {...settings}> 
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                Velit officia consequat duis enim velit mollit.
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                Velit officia consequat duis enim velit mollit.
                                                                
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d02 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                Velit officia consequat duis enim velit mollit.
                                                                
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                Velit officia consequat duis enim velit mollit.
                                                                
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                Velit officia consequat duis enim velit mollit.
                                                                
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                Velit officia consequat duis enim velit mollit.
                                                                
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                Velit officia consequat duis enim velit mollit.
                                                                
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div c lassName="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                Velit officia consequat duis enim velit mollit.
                                                               
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Slider>  
                                                </div> 
                                                //End Patient TAB's 
                                            }
                                            
                                            

                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div className="review-plus">
                        <img src={ whitePlus } />
                    </div>
                    <div className="review-plus-sm">
                        <img src={ whitePlus } />
                    </div>
                </section>
            </Fragment>
        )
    }
}

export default Reviews;