import React, { Component, Fragment } from 'react';
import OwlCarousel from 'react-owl-carousel2';
// import 'react-owl-carousel2/style.css'; 
//Allows for server-side rendering.
import iosPlayStore from './../../assets/images/iosPlayStore.png';
import googlePlayStore from './../../assets/images/googlePlayStore.png';
import bluephone from './../../assets/images/bluephone.png';
import whitephone from './../../assets/images/whitephone.png';
import whitePlus from './../../assets/images/whitePlus.png';
import CS1 from './../../assets/images/CS1.png';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
 
class DownloadApp extends Component{
    
 options = {
    items: 1,
    nav: true, 
    autoplay: true
};
 events = {
    onDragged: function(event) {

    },
    onChanged: function(event) {

    }
};

    render(){
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nav:false,
            arrows:false,
          };
        return(
            <Fragment>
                <section className="download-app">
                    <div className="container-fluid"> 
                        <div className="row">
                            <div className="col-lg-6 ios-side side">
                                <div className="donwload-apps-inner d-flex">
                                    <div className="app-text-icons"> 
                                        <Slider {...settings}>
                                            <div>
                                                <div className="app-text">
                                                    <h4>Doctors’ App</h4>
                                                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.</p>
                                                </div>
                                            </div>  
                                            <div>
                                                <div className="app-text">
                                                    <h4>Doctors’ App</h4>
                                                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.</p>
                                                </div>
                                            </div>  
                                            <div>
                                                <div className="app-text">
                                                    <h4>Doctors’ App</h4>
                                                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.</p>
                                                </div>
                                            </div>  
                                            </Slider>
                                            <div className="apps-icons">
                                                <a href="https://play.google.com/store/apps/details?id=com.doclink.doctor" className="d-block">
                                                    <img src={ googlePlayStore }  className="img-fluid" />
                                                </a>
                                                <a href="https://apps.apple.com/us/app/doclink-doctor-app/id1510926246" className="d-block">
                                                    <img src={ iosPlayStore }  className="img-fluid" />
                                                </a>
                                            </div>   
                                        </div>
                                    <div className="phone-img">
                                        <img src={ bluephone }  className="img-fluid" /> 
                                    </div>
                                </div>
                                <div className="review-plus-blue">
                                    <img src={ CS1 } />
                                </div>
                                <div className="review-plus-blue-sm">
                                    <img src={ CS1 } />
                                </div>
                            </div>

                            <div className="col-lg-6 android-side side">
                                <div className="donwload-apps-inner d-flex">
                                    <div className="phone-img">
                                        <img src={ whitephone }  className="img-fluid" />
                                    </div>
                                    <div className="app-text-icons"> 
                                        <Slider {...settings}>
                                            <div>
                                                <div className="app-text">
                                                    <h4>Patients’ App</h4>
                                                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.</p>
                                                </div>
                                            </div>  
                                            <div>
                                                <div className="app-text">
                                                    <h4>Patients’ App</h4>
                                                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.</p>
                                                </div>
                                            </div>  
                                            <div>
                                                <div className="app-text">
                                                    <h4>Patients’ App</h4>
                                                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.</p>
                                                </div>
                                            </div>  
                                        </Slider>
                                        <div className="apps-icons">
                                            <a href="https://play.google.com/store/apps/details?id=com.doclink.doctor" className="d-block">
                                                <img src={ googlePlayStore }  className="img-fluid" />
                                            </a>
                                            <a href="https://apps.apple.com/us/app/doclink-doctor-app/id1510926246" className="d-block">
                                                <img src={ iosPlayStore }  className="img-fluid" />
                                            </a>
                                        </div> 
                                    </div>
                                </div>
                                <div className="review-plus-white">
                                    <img src={ whitePlus } />
                                </div>
                                <div className="review-plus-white-sm">
                                    <img src={ whitePlus } />
                                </div>
                            </div>                            
                        </div>
                    </div>
                </section>
            </Fragment>
        )
    }
}


export default DownloadApp;