import React, { Component, Fragment } from 'react';
import docktor from './../../assets/images/docktor.png';


class Faq extends Component{
    render(){
        return(
            <Fragment>
                <section className="contact-faq-wrapper">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-8">
                                <div className="col-md-8 faq-left-wrapper">
                                    <h3 className="sub-heading">Have a Question?</h3>
                                    <h1 className="section-heading">Let’s Get Answers</h1>
                                </div>
                            </div>
                            <div className="col-md-7">
                            <div id="accordion"> 
                                <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingFive">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                        <div className="toggle-btn">
                                                            <span class="plus circle">+</span>
                                                            <span class="sub circle">-</span> 
                                                        </div>
                                                        How can I register with DocLink?
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                                <div class="card-body">
                                                    You can access the Doclink platform after downloading the DocLink Patient App from Playstore.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                        <div className="toggle-btn">
                                                            <span class="plus circle">+</span>
                                                            <span class="sub circle">-</span> 
                                                        </div>
                                                        Which type of specialists are available on DocLink?
                                                    </button>
                                                </h5>
                                            </div> 
                                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                Patients can find many different specialists on Doclink including gastroenterologist, urologist, diabetologist, etc.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingTwo">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                        <div className="toggle-btn">
                                                            <span class="plus circle">+</span>
                                                            <span class="sub circle">-</span> 
                                                        </div>
                                                        How will I pay for DocLink services?
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapseTwo" class="collapse " aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body">
                                                    Patients have to top up their in-app wallet to use Doclink services. Currently, the patients can top-up their account only through bank transfer, however, soon more payment channels will be made available.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingThree">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                        <div className="toggle-btn">
                                                            <span class="plus circle">+</span>
                                                            <span class="sub circle">-</span> 
                                                        </div>
                                                         Do your packages have any time validity?
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                <div class="card-body">
                                                No, our packages have no time validity.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingFour">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                        <div className="toggle-btn">
                                                            <span class="plus circle">+</span>
                                                            <span class="sub circle">-</span> 
                                                        </div>
                                                        Does the app have online payment facility?
                                                    </button>
                                                </h5>
                                            </div>
                                            <a id="contact-us"></a>
                                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                                <div class="card-body">
                                                No, the app doesn’t have an online payment facility but soon it will be made available.
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-1"></div>

                          <div className="col-md-3">
                                  {/*  */}
                                <div className="doctor-pics">
                                    <img src={ docktor } className="img-fluid" />
                                </div>
                          </div>
                        </div>
                    </div>
                </section>
            </Fragment>
        )
    }
}


export default Faq;