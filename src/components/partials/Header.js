import React, { Component, Fragment } from 'react';
import headerLogo from './../../assets/images/headerLogo.png';
import downArrow from './../../assets/images/downArrow.png';
import sliderright from './../../assets/images/sliderright.png';
import headerblueplusmedium from './../../assets/images/headerblueplusmedium.png';
import headerplay from './../../assets/images/headerplay.png';
import whitePlus from './../../assets/images/whitePlus.png';
import lightblue from './../../assets/images/lightblue.png';


 
class Header extends Component{
    render(){
        return( 
            <Fragment>
                <header> 
                    {/* ===================
                        *** Navigation ***
                    ==========================
                     */}
                    <div className="main-navigaiton">
                        <nav className="navbar navbar-expand-lg navbar-light">
                        <a className="navbar-brand" href="#"> <img src={ headerLogo } /> </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav desktop-nav">
                                <li className="nav-item active">
                                    <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#"> Our Reviews </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#"> Download App </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">FAQ's</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Connect</a>
                                </li> 
                                </ul> 
                            </div>
                        </nav>
                    </div>
                    {/* ===================
                        *** End Navigation ***
                    ==========================
                     */}

                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-6"></div>
                            <div className="col-md-6">
                                <div className="slider-img">
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal">
                                        <img src={ sliderright } className="img-fluid" />
                                        <div className="header-play">
                                            <img src={headerplay} className="img-fluid" />
                                        </div>
                                    </a>
                                    <div className="blue-small-icon">
                                        <img src= {headerblueplusmedium} className="img-fluid" />
                                    </div>
                                    <div className="white-small-icon">
                                        <img src= {whitePlus} className="img-fluid" />
                                    </div>
                                    <div className="white-medium-icon">
                                        <img src= {whitePlus} className="img-fluid" />
                                    </div>
                                    <div className="blue-medium-icon">
                                        <img src= {headerblueplusmedium} className="img-fluid" />
                                    </div>
                                    <div className = "light-blue-medium-icon">
                                        <img src = {lightblue} className = "img-fluid" />
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>

                     {/* ===================
                        *** Slider Section ***
                    ==========================
                     */}
                        <section className="slider-section">
                            <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="banner-text-area">
                                                <div className="banner-text-sec">
                                                    <h4>A messaging platform which <span className="d-block">connects doctors to their patients</span></h4>    
                                                </div>
                                                <div className="slider-btns">
                                                    <a href="javascript:void(0)" className="common-btn light-blue-btn">Download</a>
                                                    <a href="javascript:void(0)" className="common-btn  dark-blue-btn">Connect</a>
                                                </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div className="down-arrow">
                            <a href="javascript:void()">
                                <img src={ downArrow } />
                            </a>
                        </div> 
                     </section>
                     {/* ===================
                        *** End Slider Section ***
                    ==========================
                     */}

                    <div className="mob-slider">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="slider-img">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal">
                                            <img src={ sliderright } className="img-fluid" />
                                            <div className="header-play">
                                                <img src={headerplay} className="img-fluid" />
                                            </div>
                                        </a>
                                        <div className="blue-small-icon">
                                            <img src= {headerblueplusmedium} className="img-fluid" />
                                        </div>
                                        <div className="white-small-icon">
                                            <img src= {whitePlus} className="img-fluid" />
                                        </div>
                                        <div className="white-medium-icon">
                                            <img src= {whitePlus} className="img-fluid" />
                                        </div>
                                        <div className="blue-medium-icon">
                                            <img src= {headerblueplusmedium} className="img-fluid" />
                                        </div>
                                        <div className = "light-blue-medium-icon">
                                            <img src = {lightblue} className = "img-fluid" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row mob-slider-section">
                                <div className="col-md-12">
                                    <div className="">  
                                        <div className="banner-text-area">
                                            <div className="banner-text-sec">
                                                <h4>A messaging platform which <span className="d-block">connects doctors to their patients</span></h4>    
                                            </div>    
                                        </div>        
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <div className="slider-btns d-flex justify-content-between">
                                        <a href="javascript:void(0)" className="common-btn light-blue-btn">Download</a>
                                        <a href="javascript:void(0)" className="common-btn  dark-blue-btn">Connect</a>
                                    </div>  
                                </div>
                                <div className="col-md-12">
                                    <div className="down-arrow">
                                        <a href="javascript:void()">
                                            <img src={ downArrow } />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                     
                {/* ===================
                *** Doclink Video Modal ***
                    ==========================
                */}
                <div className="doclink-video">
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                            <div class="">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <iframe width="100%"  height="100%" allow="autoplay; encrypted-media" src="https://www.youtube.com/embed/0-jtXo620Ts" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div> 
                            </div>
                        </div>
                    </div>
                </div>
                {/* ===================
                *** End Doclink Video Modal ***
                    ==========================
                */}
            </Fragment>
        )
    }
}

export default Header;