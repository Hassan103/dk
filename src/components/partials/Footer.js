import React, { Component, Fragment } from 'react';
import footerLogo from './../../assets/images/footerLogo.png';
import twitter from './../../assets/images/twitter.png';
import whatsapp from './../../assets/images/whatsapp.png';
import linkden from './../../assets/images/linkden.png';
import facebook from './../../assets/images/facebook.png';
import Connect from './../../assets/images/Connect.png';
import arrow_upward from './../../assets/images/arrow_upward.png';




import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
class Footer extends Component{
    render(){
        return(
            <Fragment>
            <footer>
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-md-5 footer-side left">
                      <div className="footer-text">
                        <div className="footer-logo">
                            <div className="f-logo">
                                <img src={footerLogo} /> 
                            </div> 
                            <p>A messaging application which enables the doctors to stay in touch with their own patients and address their medical concerns, 
                            anytime and anywhere.</p>
                            <p>*The interactions are secure, reliable and paid. Charges may vary for doctors and will depend on their consultation fees.</p>
                        </div> 
                      </div> 
                    </div>
                    <div className="col-md-7 footer-side right">
                      <div className="row">
                        <div className="col-md-5">
                          <div className="footer_links">
                            <ul className="no-default">
                              <li> <a href="javascript:void(0)"> About Us </a> </li>
                              <li> <a href="javascript:void(0)"> Terms of Use </a></li>
                              <li> <a href="javascript:void(0)"> Privacy Policy </a> </li>
                              <li> <a href="javascript:void(0)"> Refund Policy</a> </li>
                            </ul>
                          </div>
                        </div>
                        <div className="col-md-7">
                          <div className="footer_links icons-bar">
                              <ul className="no-default">
                                <li> <span className="icon icon-1"><svg width="22" fill="#006ED7" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><defs/><path d="M2.537 6l11.299 10.545a1.712 1.712 0 002.328 0L27.463 6H2.537zM1 7.3V24h28V7.3L17.53 18.009A3.696 3.696 0 0115 19c-.91 0-1.82-.33-2.53-.992L1 7.3z"/></svg></span>  info@doclink.health</li>
                                <li> <span className="icon icon-2"><svg width="22" fill="#006ED7" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><defs/><path d="M12 2a1 1 0 00-.71.297l-10.087 8.8A.5.5 0 001 11.5a.5.5 0 00.5.5H4v8a1 1 0 001 1h4a1 1 0 001-1v-6h4v6a1 1 0 001 1h4a1 1 0 001-1v-8h2.5a.5.5 0 00.5-.5.5.5 0 00-.203-.402l-10.08-8.795a1 1 0 00-.006-.006A1 1 0 0012 2z"/></svg></span> 2C Plot 8C, Sunset Lane 9, Phase 2 Commercial Area Defense Housing, Karachi, Sindh, 75500.</li>
                              </ul>
                            </div>
                        </div>
                      </div>
                    </div>  
                  </div> 
                </div>
                {/****************** Copyrights  *********************/}
                <div className="copyrights">
                    <div className="container-fluid">
                      <div className="row">
                        <div className="col-md-6">
                          <div className="copy-text">
                            <p>All rights reserved. DocLink © 2021.</p>
                          </div>  
                        </div>
                        <div className="col-md-6">
                          <div className="social-media-icons">
                              Connect with us:
                              <img src={ twitter }  />
                              <img src={ facebook }  /> 
                              <img src={ whatsapp }  />
                              <img src={ linkden }  />                              
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
                  <a href="">
                    <div className="round-connect">
                        <img src={Connect} className="img-fluid" />
                    </div>
                  </a>
                  <a href="">
                    <div className="round-bottom-top">
                        <img src={arrow_upward} className="img-fluid" />
                    </div>
                  </a>
                {/****************** End Copyrights  *********************/}
              </footer>
            </Fragment> 
        )
    }
}


export default Footer;