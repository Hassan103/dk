import React, { Component, Fragment } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './assets/css/style.css'
import Home from './components/Home';
import Footer from './components/partials/Footer';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import AboutUs from './components/AboutUs';




 
class App extends Component{
  constructor(props){
    super(props);

  }
  render(){
    return(
      <Fragment>
        <Router>
          <Switch>
            <Route path="/">
                <Home />      
            </Route>
            <Route path="/about-us">
              <AboutUs />
            </Route>
          </Switch>
        </Router>
      </Fragment>
    )
  }
}
 



export default App;